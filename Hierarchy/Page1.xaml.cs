﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            starterPicker.ItemsSource = new List<string>() { "Bulbasaur", "Charmander", "Squirtle" };
            starterPicker.SelectedIndexChanged += this.starterPickerSelectedIndexChanged;
            choose.Clicked += this.chooseStarterEvent;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
        public void starterPickerSelectedIndexChanged(object sender, EventArgs e) //Called every time when picker selection changed.
        {
            string selectedValue = starterPicker.Items[starterPicker.SelectedIndex];
            selectedValue = selectedValue.ToLower();
            starter.Source = ImageSource.FromFile(selectedValue + ".png");
        }

        public async void chooseStarterEvent(object send, EventArgs e) //Called when the choose button is clicked
        {
            if (starterPicker.SelectedIndex >= 0)
                await Navigation.PushAsync(new Page2(starterPicker.Items[starterPicker.SelectedIndex]));
        }
    }
}