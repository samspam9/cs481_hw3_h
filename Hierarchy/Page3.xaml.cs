﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        string evo = "";
        public Page3(string evolution)
        {
            InitializeComponent();
            this.evo = evolution;
            goHome.Clicked += this.goHomeEvent;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lastEvoLabel.Text = "Amazing! " + evo + " evolved again!";
            string lastEvo = "";
            switch (evo)
            {
                case "Ivysaur":
                    lastEvo = "venusaur";
                    break;
                case "Charmeleon":
                    lastEvo = "charizard";
                    break;
                case "Wartortle":
                    lastEvo = "blastoise";
                    break;
            }
            lastEvoImg.Source = ImageSource.FromFile(lastEvo + ".png");
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public async void goHomeEvent(object send, EventArgs e) //Called when the train button is clicked
        {
            await Navigation.PopToRootAsync();
        }
    }
}