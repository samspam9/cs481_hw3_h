﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        string evolution = "";
        string starter = "";
        public Page2(string starterPokemon)
        {
            InitializeComponent();
            this.starter = starterPokemon;
            train.Clicked += trainEvent;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            evolvedLabel.Text = "Great! " + starter + " evolved!";
            string evo = "";
            switch (starter)
            {
                case "Bulbasaur":
                    evo = "ivysaur";
                    break;
                case "Charmander":
                    evo = "charmeleon";
                    break;
                case "Squirtle":
                    evo = "wartortle";
                    break;
            }
            this.evolution = char.ToUpper(evo[0]) + evo.Substring(1);
            starterEvo.Source = ImageSource.FromFile(evo + ".png");
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public async void trainEvent(object send, EventArgs e) //Called when the train button is clicked
        {
            await Navigation.PushAsync(new Page3(this.evolution));
        }
    }
}