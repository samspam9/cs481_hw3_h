﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Page1()) {
                BarBackgroundColor = Color.FromHex("#CC0000"),
                BarTextColor = Color.White
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
